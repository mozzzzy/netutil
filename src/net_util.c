/*
 *                                                                 ___   ___
 *                         ___                              ___   |\__\ |\  \
 *  ________     _______   |\  \__               ___ ____   |\  \__\| _|_\ \  \
 * |\   ___  \ |\   __  \ |\_    _\             |\  \|\  \ |\_    _\ |\  \\ \  \
 * \ \  \\ \  \\ \  _____\\|__\  \|_   ________ \ \  \_\  \\|__\  \|_\ \  \\ \  \
 *  \ \__\\ \__\\ \_______\  \ \____\ |\________\\ \_______\  \ \____\\ \__\\ \__\
 *   \|__| \|__| \|_______|   \|____| \|________| \|_______|   \|____| \|__| \|__|
 *
 * Copyright 2018 Takuya Kitano
 * Utility of TCP/UDP for c.
 */
#include <string.h>       // memset, strlen
#include <stdio.h>        // snprintf
#include <stdlib.h>       // malloc
#include <netdb.h>        // struct addrinfo
#include <arpa/inet.h>    // inet_ntop
#include <unistd.h>       // close, write
#include "net_util.h"

int WRITE_BUF_SIZE = 1024;

/*
 * resolve
 *
 * [Description]
 * Getting fqdn and port, this function resolves them to ip address,
 * and return addrinfo structure.
 * NOTE : We should execute `freeaddrinfo` function when we finish using
 *        the addrinfo structure that is returned by this function.
 *
 * [Parameter]
 * - char *fqdn              : pointer of fqdn string.
 * - unsigned short int port : port number.
 *
 * [Return Value]
 * - If success : struct addrinfo *res  : This contains resolved ip address.
 * - Otherwise  : NULL
 */
struct addrinfo *resolve(char *fqdn, unsigned short int port, int sock_type, int family) {
  struct addrinfo *res;
  struct addrinfo hints;
  memset(&hints, 0, sizeof(hints));
  hints.ai_socktype = sock_type;
  hints.ai_family = family;

  char port_chr[5];
  snprintf(port_chr, 5, "%d", port);

  int err;
  if ((err = getaddrinfo(fqdn, port_chr, &hints, &res)) != 0) {
    return NULL;
  }

  return res;
}


/*
 * getAddr
 *
 * [Description]
 * Getting pointer of addrinfo structure, this function returns
 * pointer of character whose value is ip address string.
 * NOTE : We should execute `free` when we finish using returned ip_addr.
 *
 * [Parameter]
 * - struct addrinfo *res : This contains resolved ip address data.
 *
 * [Return Value]
 * - char *ip_addr
 */
char *getAddr(struct addrinfo *res) {
  // 16 = strlen("000,000,000,000")
  char *ip_addr = (char *) (malloc(sizeof(char) * 16));
  struct in_addr addr;
  addr.s_addr = ((struct sockaddr_in *)(res->ai_addr))->sin_addr.s_addr;
  inet_ntop(AF_INET, &addr, ip_addr, 16);

  return ip_addr;
}


/*
 * connectTo
 *
 * [Description]
 * Getting pointer of addrinfo structure, this function connects to specified host.
 * NOTE : This function executes `freeaddrinfo(res)` to deallocate res.
 *
 * [Parameter]
 * - struct addrinfo *res : This contains host data to connect.
 *
 * [Return Value]
 * - If connect successfully : int socket
 * - Otherwise               : ERROR
 */
int connectTo(struct addrinfo *res) {
  int sock = -1;
  struct addrinfo *resc;
  for (resc=res; resc != NULL; resc=resc->ai_next) {
    // get socket file descriptor
    sock = socket(resc->ai_family, resc->ai_socktype, resc->ai_protocol);
    // if can't get sock, try next
    if (sock < 0) {
      continue;
    }

    // if get the socket successfully, connect to server
    // if couldn't connect to server
    if (connect(sock, resc->ai_addr, resc->ai_addrlen) != 0) {
      close(sock);
      continue;

    // else if connect to server succesfully
    } else {
      break;
    }
  }
  // free res
  freeaddrinfo(res);

  // couldn't connect to server
  if (resc == NULL) {
    return ERROR;
  }

  return sock;
}


/*
 * writeFileData
 *
 * [Description]
 * Getting socket and file descriptor,
 * this function writes the file data to the socket.
 *
 * [Parameter]
 * - int sock : socket that is accepted.
 * - int file : file descriptor to send data.
 *
 * [Return Value]
 * - int write_size
 */
int writeFileData(int sock, int file, int write_len) {
  char write_buf[WRITE_BUF_SIZE];
  int read_size;
  int total_read_size = 0;
  int real_write_size = 0;
  while ((read_size = read(file, write_buf, sizeof(write_buf))) > 0) {
    if ((total_read_size + read_size) < write_len) {
      real_write_size += write(sock, write_buf, read_size);
    } else {
      real_write_size += write(sock, write_buf, write_len - total_read_size);
      break;
    }
    total_read_size += read_size;
  }
  return real_write_size;
}


/*
 * listenTo
 *
 * [Description]
 * Getting port, address_family, and socket_type,
 * this function returns socket that is listening at the specified port.
 *
 * [Parameter]
 * - unsigned short int port : e.g. 80, 8080
 * - int address_family      : e.g. AF_INET.     (More information, see man page of socket.h)
 * - int socket_type         : e.g. SOCK_STREAM. (More information, see man page of socket.h)
 *
 * [Return Value]
 * - If bind and listen successfully : int socket
 * - Otherwise                       : ERROR
 */
int listenTo(unsigned short int port, int address_family, int socket_type) {
  struct addrinfo *res, hints;
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = address_family;
  hints.ai_flags = AI_PASSIVE;
  hints.ai_socktype = socket_type;

  char port_chr[5];
  snprintf(port_chr, 5, "%d", port);

  // set info into "res"
  int err = getaddrinfo(NULL, port_chr, &hints, &res);
  // printf("%s", gai_strerror(err));

  if (err != 0) {
    return ERROR;
  }

  // create socket
  int sock = socket(res->ai_family, res->ai_socktype, 0);
  if (sock < 0) {
    return ERROR;
  }

  // set socket option
  int yes = 1;
  setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&yes, sizeof(yes));

  // bind "res" to "sock"
  if (bind(sock, res->ai_addr, res->ai_addrlen) != 0) {
    return ERROR;
  }

  // listen
  if (listen(sock, 5) != 0) {
    return ERROR;
  }

  return sock;
}


/*
 * acceptReq
 *
 * [Description]
 * Getting socket that is listening, this function waits for a request,
 * and when get a request, returns accepted socket.
 *
 * [Parameter]
 * - int listening_sock : socket that is listening.
 *
 * [Return Value]
 * - If accept Successfully : int accepted_sock
 * - Otherwise              : ERROR
 */
int acceptReq(int listening_sock) {
  struct sockaddr_in client;
  socklen_t len;
  int accepted_sock = accept(listening_sock, (struct sockaddr *)&client, &len);
  if (accepted_sock < 0) {
    return ERROR;
  }
  return accepted_sock;
}
