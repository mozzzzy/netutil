/*
 * Copyright 2018 Takuya Kitano
 */
#ifndef NET_UTIL_H_   // include guard
#define NET_UTIL_H_
enum return_code {
  ERROR = -1,
  SUCCESS
};
extern int WRITE_BUF_SIZE;

struct addrinfo *resolve(char *fqdn, unsigned short int port, int sock_type, int family);

char *getAddr(struct addrinfo *res);

int connectTo(struct addrinfo *res);

int writeFileData(int sock, int file, int write_len);

int listenTo(unsigned short int port, int address_family, int socket_type);

int acceptReq(int listening_sock);

#endif  // NET_UTIL_H_
