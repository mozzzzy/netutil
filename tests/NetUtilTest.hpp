/*
 * Copyright 2018 Takuya Kitano
 */
#include <cppunit/extensions/HelperMacros.h>  // CPPUNIT_TEST_SUITE, CPPUNIT_TEST ...

#include <string.h>     // strcmp
#include <unistd.h>     // write, close
#include <sys/stat.h>   // S_IREAD, S_IWRITE
#include <fcntl.h>      // open, O_RDWR, O_TRUNC, O_CREAT
#include <errno.h>      // errno
#include <netdb.h>      // AF_INET, SOCK_STREAM
#include <stdlib.h>     // exit

extern "C"
{
#include <net_util.h>
}

class NetUtilTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(NetUtilTest);

  // resolve function
  CPPUNIT_TEST(test_resolve);
  // connectTo function
  CPPUNIT_TEST(test_connectTo);
  // writeFileData function
  CPPUNIT_TEST(test_writeFileData);
  // listenTo function
  CPPUNIT_TEST(test_listenTo);
  // acceptReq function
  CPPUNIT_TEST(test_acceptReq);

  CPPUNIT_TEST_SUITE_END();

 public:
  void test_resolve();
  void test_connectTo();
  void test_writeFileData();
  void test_listenTo();
  void test_acceptReq();

  static const int BUF_SIZE = 1024;
};


//
// test_resolve
//
void NetUtilTest::test_resolve() {
  char fqdn[] = "localhost";
  int port = 80;
  char expected[] = "127.0.0.1";

  // resolve test
  struct addrinfo *res = resolve(fqdn, port, SOCK_STREAM, AF_INET);
  CPPUNIT_ASSERT(res != NULL);

  // getAddr test
  char *ip_addr = getAddr(res);
  CPPUNIT_ASSERT(strcmp(expected, ip_addr) == 0);
}


//
// test_connect
//
void NetUtilTest::test_connectTo() {
  // preparation
  char fqdn[] = "www.google.com";
  int port = 80;

  // preparation
  struct addrinfo *res = resolve(fqdn, port, SOCK_STREAM, AF_INET);
  CPPUNIT_ASSERT(res != NULL);

  // conntectTo test
  int connected_sock = connectTo(res);
  CPPUNIT_ASSERT(connected_sock > 0);
  close(connected_sock);
}


//
// test_writeFileData
//
void NetUtilTest::test_writeFileData() {
  char output_file[] = "./output_file.dat";
  char input_file[] = "./input_file.dat";
  char message[] = "This is a test message.";

  // preparation
  // create mock socket to write data
  int mock_sock =
    open(output_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);
  CPPUNIT_ASSERT(errno == 0);
  // create file to read data
  int input_file_descriptor =
    open(input_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);
  CPPUNIT_ASSERT(errno == 0);
  int write_size = write(input_file_descriptor, message, strlen(message));
  CPPUNIT_ASSERT(write_size == strlen(message));
  // reopen read data file
  close(input_file_descriptor);
  input_file_descriptor = open(input_file, O_RDONLY, S_IREAD | S_IWRITE);

  // writeFileData test
  write_size = writeFileData(mock_sock, input_file_descriptor, strlen(message));
  CPPUNIT_ASSERT(write_size == strlen(message));

  // close write data file
  close(mock_sock);

  // get file size
  struct stat stat_buf;
  int stat_result = stat(input_file, &stat_buf);
  // stat should be success
  CPPUNIT_ASSERT(stat_result == 0);
  // file size should be message size
  CPPUNIT_ASSERT(stat_buf.st_size == strlen(message));

  // verify wrote data
  mock_sock = open(output_file, O_RDONLY, S_IREAD);
  char read_buf[BUF_SIZE];
  int read_size = read(mock_sock, read_buf, strlen(message));
  CPPUNIT_ASSERT(strcmp(read_buf, message) == 0);

  // cleanup
  close(mock_sock);
  remove(output_file);
  close(input_file_descriptor);
  remove(input_file);
}


//
// test_listenTo
//
void NetUtilTest::test_listenTo() {
  int port = 12345;

  // test listenTo
  int listening_sock = listenTo(port, AF_INET, SOCK_STREAM);
  CPPUNIT_ASSERT(listening_sock > 0);

  // cleanup
  close(listening_sock);
}


//
// test_acceptReq
//
void NetUtilTest::test_acceptReq() {
  int test_port = 12345;
  char localhost[] = "localhost";

  if (fork() == 0) {
    // create lintening socket
    int listening_sock = listenTo(test_port, AF_INET, SOCK_STREAM);
    int accepting_sock = acceptReq(listening_sock);
  } else {
    // create tcp client
    sleep(1);
    struct addrinfo *res = resolve(localhost, test_port, SOCK_STREAM, AF_INET);
    int sock = connectTo(res);
    close(sock);
    exit(0);
  }
}
