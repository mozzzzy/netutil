#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

extern "C"
{
#include <net_util.h>
}

int main() {
  CppUnit::TextUi::TestRunner runner;
  CppUnit::TestFactoryRegistry &registry =
    CppUnit::TestFactoryRegistry::getRegistry();
  runner.addTest(registry.makeTest());

  bool wasSuccessful = runner.run();
  return !wasSuccessful;
}
